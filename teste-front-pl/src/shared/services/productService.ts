import axios, { AxiosResponse, AxiosError } from 'axios';
import { API_PRODUCT } from '../environment/ApiConstants';
import { IProduct } from '../@types';


const produto = axios.create({
  baseURL: API_PRODUCT,
  headers: {
    Accept: 'application/json',
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json',
  },
});

export async function getProduct(): Promise<AxiosResponse<IProduct | any> | AxiosError | undefined> {
    try {
      const response = await produto.get('');
      return response;
    } catch (error) {
      if(error instanceof AxiosError){      
        if (error.response) {
          return error.response;
        }
    
        return error;
      }
    }
  }

export async function deleteProduct(id: string): Promise<AxiosResponse | AxiosError | undefined> {
    try {
      const response = await produto.delete(`/${id}`);
      return response;
    } catch (error) {
      if(error instanceof AxiosError){      
        if (error.response) {
          return error.response;
        }
    
        return error;
      }
    }
  }
  


