import React, { useState } from 'react'
import {
  Box,
  Text,
  Card,
  Image,
  Stack,
  CardBody,
  Heading,
  Button,
  useDisclosure,
} from '@chakra-ui/react'
import { Rating } from 'react-simple-star-rating'
import { ViewIcon } from '@chakra-ui/icons'
import { ProdutoModal } from './ProdutoModal'
import { FiHeart, FiEye } from 'react-icons/fi'

interface ICategoriasCardProps {
  id: string
  name: string
  description: string
  price: number
  image: string
  rating_stars: number
}

export const ProdutosCard: React.FC<ICategoriasCardProps> = ({
  id,
  name,
  image,
  description,
  price,
  rating_stars,
}) => {
  const [mouseHover, setMouseHover] = useState<boolean>(false)
  const handleMouseHover = () => setMouseHover(true)
  const handleMouseOff = () => setMouseHover(false)

  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <>
      <Box>
        <Card
          maxW={300}
          size={'sm'}
          onMouseEnter={() => handleMouseHover()}
          onMouseLeave={() => handleMouseOff()}
          bgColor={'gray.100'}
          variant={'filled'}
        >
          <CardBody
            display={'flex'}
            alignItems={'center'}
            flexDirection={'column'}
          >
            <Box
              w={'full'}
              alignItems={'center'}
              flexDirection={'column'}
              display={'flex'}
              pb={7}
            >
              {mouseHover && (
                <>
                  <Button
                    position={'absolute'}
                    right={3}
                    top={5}
                    px={2}
                    py={1}
                    rounded={'full'}
                    bg='white'
                    shadow={'base'}
                    onClick={onOpen}
                  >
                    <FiEye />
                  </Button>
                  <Button
                    position={'absolute'}
                    right={3}
                    top={16}
                    px={2}
                    py={1}
                    rounded={'full'}
                    bg='white'
                    shadow={'base'}
                  >
                    <FiHeart />
                  </Button>
                </>
              )}
              <Image
                mt={4}
                src={image}
                alt={name}
                borderRadius='lg'
                w={165}
                h={200}
                objectFit={'cover'}
                mb={3}
              />
              {mouseHover && (
                <>
                  <Button
                    mt={2}
                    bgColor={'gray.700'}
                    textColor={'white'}
                    w={'100%'}
                    colorScheme='gray'
                    position={'absolute'}
                    bottom={32}
                    onClick={onOpen}
                  >
                    Quick View
                  </Button>
                  <ProdutoModal
                    id={id}
                    onClose={onClose}
                    isOpen={isOpen}
                    image={image}
                    name={name}
                    description={description}
                    price={price}
                    rating_stars={rating_stars}
                  />
                </>
              )}
            </Box>
            <Stack
              mt='6'
              spacing='3'
              direction={'column'}
              alignItems={'center'}
            >
              <Heading size='sm'>{name}</Heading>
              <Text as={'b'} fontSize='md'>
                {`$${price}`}
              </Text>
              <Box display={'flex'} gap={2} mb={2}>
                <Rating
                  onClick={() => undefined}
                  ratingValue={rating_stars}
                  size={17}
                />
                <Text mt={-0.5} textColor={'gray.600'}>
                  (12 reviews)
                </Text>
              </Box>
            </Stack>
          </CardBody>
        </Card>
      </Box>
    </>
  )
}
