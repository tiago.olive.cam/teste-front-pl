import React from 'react'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  Card,
  CardBody,
  Stack,
  Text,
  Divider,
  CardFooter,
  ButtonGroup,
  Image,
  Box,
  Spacer,
} from '@chakra-ui/react'
import { Rating } from 'react-simple-star-rating'
import { DeleteIcon } from '@chakra-ui/icons'
import { deleteProduct } from '@/shared/services/productService'
import { Router, useRouter } from 'next/router'

interface IProdutoModalProps {
  id: string
  name?: string
  description?: string
  price?: number
  image?: string
  rating_stars: number
  isOpen: boolean
  onClose: () => void
}

export const ProdutoModal: React.FC<IProdutoModalProps> = ({
  id,
  isOpen,
  name,
  price,
  image,
  description,
  rating_stars,
  onClose,
}) => {
  const router = useRouter()

  const handleDelete = (id: string) => {
    deleteProduct(id)
      .then((data) => {
        // @ts-ignore
        if (data.status === 200) {
          router.reload()
        } else {
          alert('Desculpe, Serviço temporariamente indisponível!')
        }
      })
      .catch((error) => {
        alert('Desculpe, Serviço temporariamente indisponível!')
      })
  }

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{name}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Card h={'full'} w={'full'}>
              <CardBody>
                <Image
                  src={image}
                  alt={name}
                  w={'full'}
                  h={300}
                  objectFit={'cover'}
                  borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                  <Text>{description}</Text>
                  <Text color='blue.600' fontSize='2xl'>
                    {`$${price}`}
                  </Text>
                  <Box display={'flex'} gap={2} mb={2}>
                    <Rating
                      onClick={() => undefined}
                      ratingValue={rating_stars}
                      size={17}
                    />
                    <Text mt={-0.5} textColor={'gray.600'}>
                      (12 reviews)
                    </Text>
                  </Box>
                </Stack>
              </CardBody>
              <Divider />
              <CardFooter>
                <ButtonGroup spacing='2'>
                  <Button variant='solid' colorScheme='blue'>
                    Buy now
                  </Button>
                  <Button variant='ghost' colorScheme='blue'>
                    Add to cart
                  </Button>
                  <Button
                    variant='solid'
                    colorScheme='red'
                    onClick={() => handleDelete(id)}
                  >
                    <DeleteIcon />
                  </Button>
                </ButtonGroup>
              </CardFooter>
            </Card>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}
