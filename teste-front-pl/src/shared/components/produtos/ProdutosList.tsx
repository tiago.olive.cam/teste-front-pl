import React, { useEffect, useState } from 'react'
import { ProdutosCard } from './ProdutosCard'
import { Grid, GridItem } from '@chakra-ui/react'
import { produtosMock } from './mock'
import { getProduct } from '@/shared/services/productService'
import { IProduct } from '@/shared/@types'

export const ProdutosList = () => {
  const [produtos, setProdutos] = useState<IProduct[] | undefined>(produtosMock)

  useEffect(() => {
    getProduct()
      .then((data) => {
        // @ts-ignore
        if (data.status === 200) {
          // @ts-ignore
          setProdutos(data.data)
        } else {
          alert('Desculpe, Serviço temporariamente indisponível!')
        }
      })
      .catch((error) => {
        alert('Desculpe, Serviço temporariamente indisponível!')
      })
  }, [])

  return (
    <>
      <Grid
        templateColumns={{
          base: 'repeat(1, 1fr)',
          md: 'repeat(3, 1fr)',
          lg: 'repeat(4, 1fr)',
        }}
        gap={2}
      >
        {produtos?.map((produto, index) => {
          return (
            <GridItem key={index}>
              <ProdutosCard
                id={produto.id}
                name={produto.name}
                image={produto.image}
                description={produto.description}
                price={produto.price}
                rating_stars={produto.rating_stars}
              />
            </GridItem>
          )
        })}
      </Grid>
    </>
  )
}
