export const produtosMock = [
  {
    id: "1",
    name: 'Fantastic Steel Hat',
    description:
      'The slim & simple Maple gamimg keyboar from dev byte comes with a sleek body and 7 colors RGB led black lightnihg for smart funcionality',
    price: 791,
    image:
      'https://images.pexels.com/photos/10325721/pexels-photo-10325721.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    rating_stars: 2,
  },
  {
    id: "2",
    name: 'Ergonomic Bronze Chair',
    description:
      'The slim & simple Maple gamimg keyboar from dev byte comes with a sleek body and 7 colors RGB led black lightnihg for smart funcionality',
    price: 927,
    image:
      'https://images.pexels.com/photos/2691660/pexels-photo-2691660.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    rating_stars: 4,
  },
  {
    id: "3",
    name: 'Licensed Rubber Chips',
    description:
      'The slim & simple Maple gamimg keyboar from dev byte comes with a sleek body and 7 colors RGB led black lightnihg for smart funcionality',
    price: 423,
    image:
      'https://images.pexels.com/photos/7262700/pexels-photo-7262700.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    rating_stars: 3,
  },
  {
    id: "4",
    name: 'Mouse gamer RGB',
    description:
      'The slim & simple Maple gamimg keyboar from dev byte comes with a sleek body and 7 colors RGB led black lightnihg for smart funcionality',
    price: 340,
    image:
      'https://images.pexels.com/photos/2115256/pexels-photo-2115256.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    rating_stars: 4,
  },
  {
    id: "5",
    name: 'Teclado gamer RGB wireless',
    description:
      'The slim & simple Maple gamimg keyboar from dev byte comes with a sleek body and 7 colors RGB led black lightnihg for smart funcionality',
    price: 560,
    image:
      'https://images.pexels.com/photos/3812048/pexels-photo-3812048.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
    rating_stars: 5,
  },
]
