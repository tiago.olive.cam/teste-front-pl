export const categoriasData = [
  {
    nome: 'Fashion',
    imageUrl:
      'https://images.pexels.com/photos/343720/pexels-photo-343720.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Eletrônicos',
    imageUrl:
      'https://images.pexels.com/photos/248528/pexels-photo-248528.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Casa & Jardim',
    imageUrl:
      'https://images.pexels.com/photos/35847/summer-still-life-daisies-yellow.jpg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Bikes',
    imageUrl:
      'https://images.pexels.com/photos/100582/pexels-photo-100582.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Música',
    imageUrl:
      'https://images.pexels.com/photos/2235/music-sound-communication-audio.jpg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Maquiagem',
    imageUrl:
      'https://images.pexels.com/photos/3018845/pexels-photo-3018845.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Pet',
    imageUrl:
      'https://images.pexels.com/photos/160846/french-bulldog-summer-smile-joy-160846.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Bebês',
    imageUrl:
      'https://images.pexels.com/photos/12211/pexels-photo-12211.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Presentes',
    imageUrl:
      'https://images.pexels.com/photos/842876/pexels-photo-842876.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Mercado',
    imageUrl:
      'https://images.pexels.com/photos/2228553/pexels-photo-2228553.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Automóveis',
    imageUrl:
      'https://images.pexels.com/photos/175039/pexels-photo-175039.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
  {
    nome: 'Bolsas',
    imageUrl:
      'https://images.pexels.com/photos/1038000/pexels-photo-1038000.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
  },
]
