import React, { useState } from 'react'
import { CategoriasCard } from './CategoriasCard'
import { Grid, GridItem } from '@chakra-ui/react'
import { categoriasData } from './data'

export const CategoriasList = () => {
  const [categorias, setCategorias] = useState(categoriasData)
  return (
    <>
      <Grid
        templateColumns={{
          base: 'repeat(2, 1fr)',
          sm: 'repeat(2, 1fr)',
          md: 'repeat(3, 1fr)',
          lg: 'repeat(4, 1fr)',
          xl: 'repeat(6, 1fr)',
        }}
        gap={2}
      >
        {categorias?.map((categoria, index) => {
          return (
            <GridItem key={index}>
              <CategoriasCard
                name={categoria.nome}
                image={categoria.imageUrl}
              />
            </GridItem>
          )
        })}
      </Grid>
    </>
  )
}
