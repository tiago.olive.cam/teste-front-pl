import { ChevronRightIcon, DragHandleIcon } from '@chakra-ui/icons'
import { Box, Text, Spacer, Link } from '@chakra-ui/react'
import React from 'react'

export const CategoriasContainer = () => {
  return (
    <Box display={'flex'}>
      <Box display={'flex'} gap={2}>
        <DragHandleIcon color={'red.700'} my={2} />
        <Text fontSize={'xl'} as={'b'} color={'gray.600'}>
          Categorias
        </Text>
      </Box>
      <Spacer />
      <Link href={'/'}>
        <Box display={'flex'} gap={1}>
          <Text fontSize={'sm'} color={'gray.500'}>
            Ver todas
          </Text>
          <ChevronRightIcon mt={1} color={'gray.500'} />
        </Box>
      </Link>
    </Box>
  )
}
