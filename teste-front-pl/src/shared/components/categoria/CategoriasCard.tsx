import React, { useState } from 'react'
import {
  Box,
  Text,
  Spacer,
  Card,
  Image,
  Stack,
  CardBody,
  Heading,
  CardFooter,
  Button,
} from '@chakra-ui/react'
import Link from 'next/link'

interface ICategoriasCardProps {
  name: string
  image: string
}

export const CategoriasCard: React.FC<ICategoriasCardProps> = ({
  name,
  image,
}) => {
  return (
    <>
      <Box w={{ base: 28, sm: 48 }}>
        <Link href={''}>
          <Card
            h={{ base: 36, sm: 16 }}
            boxShadow='base'
            direction={{ base: 'column', sm: 'row' }}
            overflow='hidden'
            variant='outline'
            alignItems={'center'}
          >
            <Image
              rounded={'8%'}
              m={2}
              objectFit='cover'
              h={{ base: 16, sm: 12 }}
              w={{ base: 20, sm: 12 }}
              src={image}
              alt={name}
            />
            <CardBody>
              <Text as={'b'}>{name}</Text>
            </CardBody>
          </Card>
        </Link>
      </Box>
    </>
  )
}
