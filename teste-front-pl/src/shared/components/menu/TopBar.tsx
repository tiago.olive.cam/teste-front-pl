import React from 'react'
import { Box, Text } from '@chakra-ui/react'
import { Link } from '@chakra-ui/next-js'
import { FiShoppingCart, FiUser } from 'react-icons/fi'

export const TopBar = () => {
  return (
    <>
      <Box
        px={7}
        h={14}
        display={'flex'}
        alignItems={'center'}
        justifyContent={'center'}
        bg={'white'}
      >
        <Box display={'flex'} alignItems={'center'} gap={2}>
          <Link href={'/'}>
            <FiUser />
          </Link>
          <Link href={'/'}>
            <Box>
              <Box
                position={'absolute'}
                mb={-3}
                ml={2}
                rounded={'full'}
                bg='red.500'
              >
                <Text px={1} fontSize={10} color={'white'}>
                  1
                </Text>
              </Box>
              <FiShoppingCart />
            </Box>
          </Link>
        </Box>
      </Box>
    </>
  )
}
