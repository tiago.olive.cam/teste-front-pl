import React from 'react'
import {
  Box,
  Spacer,
  Text,
  Image,
  Menu,
  MenuButton,
  Button,
  Icon,
  Show,
} from '@chakra-ui/react'
import { ChevronDownIcon } from '@chakra-ui/icons'
import { FiInstagram, FiFacebook, FiTwitter } from 'react-icons/fi'
import { TopBar } from './TopBar'

export const MainMenu = () => {
  return (
    <>
      <Box
        px={7}
        h={14}
        display={'flex'}
        alignItems={'center'}
        justifyContent={'center'}
        bg={'gray.800'}
      >
        <Box display={'flex'} alignItems={'center'} gap={2}>
          <Box px={2} py={1} rounded={'full'} bg='red.500'>
            <Text color={'white'}>HOT</Text>
          </Box>
          <Text color={'white'}>Frete grátis em todo site</Text>
        </Box>
        <Spacer />
        <Box display={'flex'} alignItems={'center'}>
          <Box display={'flex'} alignItems={'center'} gap={2}>
            <Menu>
              <MenuButton
                _hover={{
                  background: 'transparent',
                  color: 'teal.500',
                }}
                _active={{
                  background: 'transparent',
                }}
                bgColor={'gray.800'}
                as={Button}
                rightIcon={<ChevronDownIcon color={'white'} />}
              >
                <Box display={'flex'} gap={2}>
                  <Image
                    h={5}
                    w={5}
                    src={'https://i.redd.it/9amlos9487sz.jpg'}
                    borderRadius={'20%'}
                  />
                  <Text color={'white'}>USD</Text>
                </Box>
              </MenuButton>
            </Menu>
          </Box>
          <Show above='md'>
            <Box display={'flex'} gap={2}>
              <FiInstagram color='white' />
              <FiFacebook color='white' />
              <FiTwitter color='white' />
            </Box>
          </Show>
        </Box>
      </Box>
      <TopBar />
    </>
  )
}
